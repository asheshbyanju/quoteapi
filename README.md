# README

This README would normally document whatever steps are necessary to get the
application up and running.

* Ruby version 
Install ruby version 2.3.3p222

* System dependencies
Install Postgres and start the PostgreSQL server in the foreground

Clone the repository and get inside it:
git clone git@bitbucket.org:asheshbyanju/quoteapi.git

* From the terminal, from your application directory run following commands:

-Run bundle install

-Run rails db:create

-Run rails db:migrate to create table

-Run rails db:seed to load data from csv.

-Run rails s to start server

-goto http://localhost:3000/ in your browser
