class QuotesController < ApplicationController
	
	def quote
		count = Quote.count
		random_offset = rand(1..count)
		@random_quote = Quote.offset(random_offset).first	
	end
	
end
